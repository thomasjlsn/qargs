#!/usr/bin/env python3

from setuptools import setup

setup(
    name='quick-arguments',
    version='0.0.4',
    description='A simple argument parser',
    long_description='Check out the README on [GitLab](https://gitlab.com/thomasjlsn/qargs)!',
    long_description_content_type='text/markdown',
    author='Thomas Ellison',
    author_email='thomasjlsn@gmail.com',
    url='https://gitlab.com/thomasjlsn/qargs',
    packages=['qargs'],
    license_files=['LICENSE'],
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3',
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
    ]
)
