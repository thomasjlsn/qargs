# Copyright (C) 2022 Thomas Ellison
# Licensed under the GPL: https://www.gnu.org/licenses/gpl-3.0.txt
# For details: https://gitlab.com/thomasjlsn/qargs/-/blob/main/LICENSE

from qargs._qargs import parse_args
