#!/usr/bin/env python3

from qargs import parse_args


# ==========================================================================
# Only short args

spec = [
    ['c'],
    ['v'],
]

# Test individual short args
argv = ['-v', '-c']
args = parse_args(spec, argv)

assert args.c is True
assert args.v is True

# Test grouped short args
argv = ['-vc']
args = parse_args(spec, argv)

assert args.c is True
assert args.v is True


# ==========================================================================
# Only long args

spec = [
    ['color'],
    ['verbose'],
]

argv = ['--verbose', '--color']
args = parse_args(spec, argv)

assert args.color is True
assert args.verbose is True

# Long arguments should be able to have dashes in them: dashed will be
# converted to underscores
spec = [
    ['has-dashes'],
]

argv = ['--has-dashes']
args = parse_args(spec, argv)

assert args.has_dashes is True


# ==========================================================================
# Short and long args

spec = [
    ['c', 'color'],
    ['v', 'verbose'],
]

argv = ['--color', '-v']
args = parse_args(spec, argv)

assert args.color is True
assert args.verbose is True


# ==========================================================================
# Capturing an argument

spec = [
    ['c', 'color'],
    ['f', 'file', 1],
    ['v', 'verbose'],
]

argv = ['--color', '-f', 'test.py']
args = parse_args(spec, argv)

assert args.color is True
assert args.verbose is False
assert args.file == 'test.py'
assert not args.files
assert args.stdin is False

# Capturing an argument with grouped short args
argv = ['-cf', 'test.py']
args = parse_args(spec, argv)

assert args.color is True
assert args.verbose is False
assert args.file == 'test.py'
assert not args.files
assert args.stdin is False


# ==========================================================================
# Stdin "-" argument

argv = ['--color', '-']
args = parse_args(spec, argv)

assert args.stdin is True


# ==========================================================================
# Operands

# Position independent perands
argv = ['run', 'test', '--color', '-cv', '-f', 'test.py', '-']
args = parse_args(spec, argv)

assert args.color is True
assert args.verbose is True
assert args.file == 'test.py'
assert not args.files
assert args.stdin is True
assert args.operands == ['run', 'test']

# Everything after "--" is an operand
argv = ['-cv', '--', '-f', 'test.py']
args = parse_args(spec, argv)

assert args.color is True
assert args.verbose is True
assert not args.file
assert args.files == ['test.py']
assert args.operands == ['-f', 'test.py']

# Everything after "--" (including a dash "-") is an operand
argv = ['-cv', '--', '-f', 'test.py', 'qargs/', '-']
args = parse_args(spec, argv)

assert args.color is True
assert args.verbose is True
assert not args.file
assert args.files == ['test.py']
assert args.dirs == ['qargs/']
assert args.stdin is False
assert args.operands == ['-f', 'test.py', 'qargs/', '-']

# Files that are captured are not in operands
argv = ['-cv', '-f', 'test.py', '--', 'qargs/', '-']
args = parse_args(spec, argv)

assert not args.files
assert args.dirs == ['qargs/']
assert args.stdin is False
assert args.operands == ['qargs/', '-']


# ==========================================================================
# The spec can be defined with optional dashes in the arguments

spec = [
    ['-a', '--all'],
]

argv = ['-a']
args = parse_args(spec, argv)

assert args.all is True


# ==========================================================================
# A callable can be used to validate an argument that is captured

spec = [
    ['a', int],
]

argv = ['-a', '1']
args = parse_args(spec, argv)

assert args.a == 1


# ==========================================================================
# All numbers captured are converted to their python counterparts

spec = [
    ['num', int],
    ['hex', hex],
    ['bin', bin],
    ['oct', oct],
    ['sci', float],
    ['c', 'complex', complex],
]

argv = [
    '--num', '2',
    '--bin', '0b0101',
    '--hex', '0x2a',
    '--oct', '0o27',
    '--sci', '2e+2',
    '-c',    '4+2j',
]
args = parse_args(spec, argv)

assert args.num == 2
assert args.bin == 0b101
assert args.bin == 5
assert args.hex == 0x2a
assert args.hex == 42
assert args.oct == 0o27
assert args.oct == 23
assert args.sci == 2e+2
assert args.sci == 200.0
assert args.complex == 4+2j


# ==========================================================================
# Invalid spec names raise a ValueError

try:
    spec = [['invalid$']]
    argv = []
    args = parse_args(spec, argv)
except ValueError:
    pass
else:
    exit(1)

try:
    spec = [['2']]
    argv = []
    args = parse_args(spec, argv)
except ValueError:
    pass
else:
    exit(1)


# ==========================================================================
# An incorrect number of items in a spec raises a ValueError

# Too many items
try:
    spec = [['a', 'all', True, 'help string', 'extra-item']]
    args = parse_args(spec)
except ValueError:
    pass
else:
    exit(1)

# Not enough items
try:
    spec = [[]]
    args = parse_args(spec)
except ValueError:
    pass
else:
    exit(1)

# But a completely empty spec is OK, it's the same as not providing a
# spec to begin with, although there is no reason to do this.
spec = []
args = parse_args(spec)


# ==========================================================================
# option_args that look like arguments are accepted

spec = [['a', str]]
argv = ['-a', '-c -l 10']
args = parse_args(spec, argv)

assert args.a == '-c -l 10'

spec = [['a', str]]
argv = ['-a', '-c']
args = parse_args(spec, argv)

assert args.a == '-c'

spec = [['a', 'arguments', str]]
argv = ['--arguments', '-c']
args = parse_args(spec, argv)

assert args.arguments == '-c'
